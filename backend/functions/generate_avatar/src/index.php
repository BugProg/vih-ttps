<?php

use Appwrite\Client;

require_once 'vendor/autoload.php';

return function($req, $res) {
        $client = new Client();
        $client
            ->setEndpoint("https://appwrite.home-cloud.fr/v1") // L'URL de notre API
            ->setProject("6388e7ac53bf23b5b4e4")
            ->setKey($req['variables']['SECRET_KEY'])
            ->setSelfSigned(true);

        $data = $req['variables']['APPWRITE_FUNCTION_DATA'];

        // Crée la requete pour l'API d'OpenAI
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, 'https://api.openai.com/v1/images/generations');
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"prompt\": \"$data\",\n  \"n\": 1,\n  \"size\": \"256x256\"\n}");
         $headers = array();
         $headers[] = 'Content-Type: application/json';
         $headers[] = 'Authorization: Bearer '. $req['variables']['OPENAI_API_KEY'];
         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
         $result = json_decode(curl_exec($ch));
         curl_close($ch);

         // Récupère l'URL de l'image générée
         $url = $result->data[0]->url;

        $res->send($url);
};




