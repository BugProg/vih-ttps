<?php

use Appwrite\Client;

// You can remove imports of services you don't use
use Appwrite\Services\Account;
use Appwrite\Services\Avatars;
use Appwrite\Services\Databases;
use Appwrite\Services\Functions;
use Appwrite\Services\Health;
use Appwrite\Services\Locale;
use Appwrite\Services\Storage;
use Appwrite\Services\Teams;
use Appwrite\Services\Users;

require_once 'vendor/autoload.php';

    return function($req, $res) {

    $client = new Client();

    $client
        ->setEndpoint("https://appwrite.home-cloud.fr/v1") // L'URL de notre API
        ->setProject("6388e7ac53bf23b5b4e4")
        ->setKey($req["variables"]["APPWRITE_FUNCTION_KEY"])
        ->setSelfSigned(true);

    $ch = curl_init();

    // Récupère la question saisie par l'utilisateur
    $question = $req['variables']['APPWRITE_FUNCTION_DATA'];

    // Et crée la requête à envoyer à OpenAI
    curl_setopt($ch, CURLOPT_URL, 'https://api.openai.com/v1/completions');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"model\": \"text-davinci-003\",\n  \"prompt\": \"Q:Bonjour, comment allez-vous ?nA: Je vais bien, merci. Et vous ?nQ: Je vais bien aussi. Merci.nA:Parfait, comment puis-je vous aider?nQ:".$question."nA:\",\n  \"temperature\": 0,\n  \"max_tokens\": 1000,\n  \"top_p\": 1,\n  \"frequency_penalty\": 0.0,\n  \"presence_penalty\": 0.0}");


    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Authorization: Bearer '.$req["variables"]["OPENAI_KEY"];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    // Renvoie la réponse de l'IA
    $res -> json(json_decode($result)->choices[0]->text);
};
