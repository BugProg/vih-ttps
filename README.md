# VIH-TTPS

Ce projet a été réalisé dans le cadre de la Nuit de l'info 2022. Un concours national visant à développer une
application web en une nuit.

## Lien du site

https://nuit.home-cloud.fr/

## Sujet

Développer un site web permettant de prévenir la population sur les MST/IST, les moyens de se protéger et se faire
dépister.

## De l'IA dans le site !

Pour réaliser le défi "Mettez l’IA de DALL-E dans votre projet !". Nous avons intégré de l'intelligence artificielle
grâce à l'API de OPENAI.
Nous avons mis en place un chatbot, accessible en cliquant sur le bouton en bas à droite. Il permet de répondre à la
plupart des questions qu'un utilisateur peut se poser sur les MST, ou tout autre sujet.
De plus, nous avons fait une génération d'avatar grâce à une chaîne de caractères saisie par l'utilisateur à la création
du compte.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change
any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also
use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
