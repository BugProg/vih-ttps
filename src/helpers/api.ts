import {Account, Client, Databases, Functions, Storage} from 'appwrite';
import {environment} from '../environments/environment';

export class Api {
  private static sdk: Client | null;
  private static database: Databases;
  private static function: Functions;
  private static storage: Storage;
  private static account: Account;

  static client() {
    if (this.sdk) {
      return this.sdk;
    }

    const client = new Client();
    client
      .setEndpoint(environment.serverEndpoint)
      .setProject(environment.projectId)
      .setLocale('fr-FR');
    this.sdk = client;

    // const account = new Account(this.sdk);
    // account.listSessions().then(() => {
    //   console.log('Anonymous session already exist !')
    // }, () => {
    //   account.createAnonymousSession().then(() => {
    //     console.log('Anonymous session created !');
    //   }, (err) => {
    //     console.error('Failed to create anonymous session : ', err);
    //     account.deleteSessions().then();
    //   });
    // });

    return this.sdk;
  }

  static databases(): Databases {
    if (!this.sdk) {
      this.client();
    }
    if (this.database) {
      return this.database;
    }

    this.database = new Databases(this.sdk!);

    return this.database;
  }

  static functions(): Functions {
    if (!this.sdk) {
      this.client();
    }
    if (this.function) {
      return this.function;
    }

    this.function = new Functions(this.sdk!);

    return this.function;
  }

  static accounts(): Account {
    if (!this.sdk) {
      this.client();
    }
    if (this.account) {
      return this.account;
    }

    this.account = new Account(this.sdk!);
    return this.account;
  }

  static storages(): Storage {
    if (!this.sdk) {
      this.client();
    }
    if (this.storage) {
      return this.storage;
    }

    this.storage = new Storage(this.sdk!);

    return this.storage;
  }
}
