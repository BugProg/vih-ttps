import {Component, OnInit} from '@angular/core';
import {SeoService} from '../services/seo.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  values = [
    {
      title: 'ABOUT.values.empowerOthers.title',
      text:  'ABOUT.values.empowerOthers.text'
    },
    {
      title: 'ABOUT.values.buildWithEmpathy.title',
      text:  'ABOUT.values.buildWithEmpathy.text'
    },
    {
      title: 'ABOUT.values.viewLarge.title',
      text:  'ABOUT.values.viewLarge.text'
    }
  ]

  constructor(private seoService: SeoService) {
  }

  ngOnInit(): void {
    this.seoService.generateTags({
      title: 'Qui sommes nous',
      description: 'À propos de ABC du Plombier'
    });
  }

}
