import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LegalNoticeComponent} from './legal-notice/legal-notice.component';
import {AboutComponent} from './about/about.component';
import {BathroomComponent} from './bathroom/bathroom.component';
import {HeatingComponent} from './heating/heating.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';

const routes: Routes = [
  {path: '', component: HomeComponent, pathMatch: 'full'},
  {path: 'mentions-legales', component: LegalNoticeComponent},
  {path: 'qui-sommes-nous', component: AboutComponent},
  {path: 'quiz', component: BathroomComponent},
  {path: 'se-depister', component: HeatingComponent},
  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignupComponent},
  {path: '**', pathMatch: 'full', redirectTo: '/'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
