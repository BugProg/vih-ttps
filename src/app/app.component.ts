import {Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {LanguageService} from './services/language.service';
import {SeoService} from './services/seo.service';
import {NavigationEnd, Router} from '@angular/router';
import {filter} from 'rxjs/operators';
import {isPlatformBrowser} from '@angular/common';

declare let AOS: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    private languageService: LanguageService,
    private seoService: SeoService,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: Object) {
  }

  ngOnInit() {
    this.languageService.setInitialAppLanguage();
    if (isPlatformBrowser(this.platformId)) {
      AOS.init({
        once: true,
        duration: 800
      });
    } else {
      AOS.init({
        disable: true
      })
    }

    this.seoService.createCanonicalLink();
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
    ).subscribe((val: any) => {
      console.log(val.url)
      this.seoService.updateCanonicalLink(val.url)
    })
  }
}
