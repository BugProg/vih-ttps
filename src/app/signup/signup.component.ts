import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {LoginService} from '../services/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  email: string = '';
  password: string = '';
  ppKeyWord: string = '';
  loading: Boolean = false;
  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
  }

  onSignup() {
    console.log(this.email + ' ' + this.password + ' ' + this.ppKeyWord)
    if (this.email.length <= 0 || this.password.length <= 0 || this.ppKeyWord.length <= 0) {
      return;
    }
    this.loading = true;
    this.loginService.signup(this.email, this.password, this.ppKeyWord).then(
      res => {
        this.loading = false;
        console.log('Account crated ', res);
        this.router.navigate(['/login']);
      }, error => {
        this.loading = false;
        console.log(error);
      }
    )
  }

}
