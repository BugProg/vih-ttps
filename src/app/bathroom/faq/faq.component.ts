import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

  accordionItems =
    [
      {
        title: 'BATHROOM.accordion.item1.title',
        text: 'BATHROOM.accordion.item1.text'
      },
      {
        title: 'BATHROOM.accordion.item2.title',
        text: 'BATHROOM.accordion.item2.text'
      },
      {
        title: 'BATHROOM.accordion.item3.title',
        text: 'BATHROOM.accordion.item3.text'
      },
    ]

  constructor() {
  }

  ngOnInit(): void {
  }

}
