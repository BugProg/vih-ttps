import {Component, OnInit} from '@angular/core';
import {SeoService} from '../services/seo.service';

@Component({
  selector: 'app-bathroom',
  templateUrl: './bathroom.component.html',
  styleUrls: ['./bathroom.component.css']
})
export class BathroomComponent implements OnInit {

  makeDifferent =
    [
      {
        title: 'BATHROOM.makeDifferent.item1.title',
        subtitle: 'BATHROOM.makeDifferent.item1.subTitle',
        iconPath: '../assets/icon/medal.png',
        iconName: 'logo medaille'
      },
      {
        title: 'BATHROOM.makeDifferent.item2.title',
        subtitle: 'BATHROOM.makeDifferent.item2.subTitle',
        iconPath: '../assets/icon/trust.png',
        iconName: 'logo de confiance'
      },
      {
        title: 'BATHROOM.makeDifferent.item3.title',
        subtitle: 'BATHROOM.makeDifferent.item3.subTitle',
        iconPath: '../assets/icon/eco-friendly.png',
        iconName: 'logo de eco responsable'
      },
      {
        title: 'BATHROOM.makeDifferent.item4.title',
        subtitle: 'BATHROOM.makeDifferent.item4.subTitle',
        iconPath: '../assets/icon/saving.png',
        iconName: 'logo de cochon argent'
      }
    ]

  constructor(private seoService: SeoService) {
  }

  ngOnInit(): void {
    this.seoService.generateTags({
      title: 'Salle de Bain',
      description: 'Vous envisagez de rénover la salle de bain de votre maison à Nantes ? ABC du Plombier est un spécialiste de la rénovation des salles de bains de Nantes. Appelez-nous.'
    })
  }

}
