import {Component, OnInit} from '@angular/core';
import {LoginService} from '../services/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLogged: boolean = false;
  email: string = '';

  imgPP: string = '';
  constructor(private loginService: LoginService) {
  }

  ngOnInit(): void {
    this.loginService.isLogged.subscribe(value => {
      this.isLogged = value;
      console.log('logged')
      if (value) {
        this.loginService.getUser().then(res => {
          this.email = res.email;
        });
        this.loginService.getPP().then((res: any) => {
          console.log(res.url);
          this.imgPP = res.url;
        })
      }
    })
  }

}
