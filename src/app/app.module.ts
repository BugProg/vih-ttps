import {NgModule} from '@angular/core';
import {SwiperModule} from 'swiper/angular';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {MatInputModule} from '@angular/material/input';
import {AppRoutingModule} from './app-routing.module';
import {AboutComponent} from './about/about.component';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {BrowserModule} from '@angular/platform-browser';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {ContactComponent} from './contact/contact.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BathroomComponent} from './bathroom/bathroom.component';
import {SnackbarComponent} from './snackbar/snackbar.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ReviewsComponent} from './home/reviews/reviews.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {LegalNoticeComponent} from './legal-notice/legal-notice.component';
import {LazyLoadImagesDirective} from './shared/lazy-load-images.directive';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AchievementsComponent} from './home/achievements/achievements.component';
import {ContactDialogComponent} from './contact/contact-dialog/contact-dialog.component';
import {QuotationDialogComponent} from './quotation-dialog/quotation-dialog.component';
import {FaqComponent} from './bathroom/faq/faq.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatListModule} from '@angular/material/list';
import {HeatingComponent} from './heating/heating.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ReviewsComponent,
    FooterComponent,
    LegalNoticeComponent,
    AboutComponent,
    LazyLoadImagesDirective,
    ContactComponent,
    ContactDialogComponent,
    BathroomComponent,
    QuotationDialogComponent,
    SnackbarComponent,
    AchievementsComponent,
    FaqComponent,
    HeatingComponent,
    LoginComponent,
    SignupComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'serverApp'}),
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    HttpClientModule,
    SwiperModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatSnackBarModule,
    MatListModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    ReactiveFormsModule,
    FormsModule,
    MatExpansionModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
