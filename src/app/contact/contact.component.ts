import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ContactDialogComponent} from './contact-dialog/contact-dialog.component';
import {BotService} from '../services/bot.service';
import { HostListener } from '@angular/core';
import e from 'express';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
   if (event.target == document.getElementById("chat-input") && event.key == "Enter") this.sendMessage();
  }

  message: string = '';
  messages: { user: string, message: string }[] = [];
  isOpened: boolean = false;

  isLoading: boolean = false;

  constructor(public dialog: MatDialog, private botService: BotService) {
  }

  ngOnInit(): void {
  }

  openContactDialog() {
    this.dialog.open(ContactDialogComponent);
  }

  openChatBot() {
    this.dialog.open(ContactDialogComponent);
  }

  toggleOpen() {
    this.isOpened = !this.isOpened;
  }

  sendMessage() {
    this.isLoading = true;
    if (this.message.length) {
      this.messages.push({user: 'user', message: this.message});
      this.botService.sendMessage(this.message).then(
        res => {
          this.messages.push({user: 'bot', message: res.response});
          this.isLoading = false;
        }
      );
      this.message = '';
    }
  }
}
