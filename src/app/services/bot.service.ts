import {Injectable} from '@angular/core';
import {Api} from '../../helpers/api';

@Injectable({providedIn: 'root'})
export class BotService {

  sendMessage(message: string) {
    return Api.functions().createExecution('63894d84de87e9264b13', String(message));
  }
}
