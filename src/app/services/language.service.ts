import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable({providedIn: 'root'})
export class LanguageService {

  constructor(
    private translate: TranslateService) {
  }

  /**
   * Initialize languages at application startup.
   */
  setInitialAppLanguage() {
      this.translate.setDefaultLang('fr');
      this.translate.use('fr');
  }
}
