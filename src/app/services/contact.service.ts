import {Injectable} from '@angular/core';
import {Api} from '../../helpers/api'
import {ContactForm} from '../contact/contact-dialog/contact-dialog.component';

@Injectable({providedIn: 'root'})
export class ContactService {

  functionID = '62b8c5f021ee2f530ab6'

  /**
   * Send a message by email.
   * @param data data needed to send the email
   */
  sendMessage(data: ContactForm) {
    return Api.functions().createExecution(
      this.functionID,
      JSON.stringify(data),
      false);
  }
}
