import {Injectable} from '@angular/core';
import {Api} from '../../helpers/api';
import {BehaviorSubject} from 'rxjs';


@Injectable({providedIn: 'root'})
export class LoginService {

  isLogged = new BehaviorSubject<boolean>(false);

  log(email: string, password: string) {
    return Api.accounts().createEmailSession(email, password).then(res => {
      this.isLogged.next(true);
    });
  }

  signup(email: string, password: string, ppKeyWord: string) {
    return Api.accounts().create('unique()', email, password).then(
      res => {
        const userId = res.$id;
        console.log('Exec');
        return Api.functions().createExecution('63891216698e56cd2124', String(ppKeyWord)).then(
          res => {
            console.log(res.response);
            Api.databases().createDocument('6388eaa9180af7d3aec7', '6388eab14a30e493837d', String(userId), {url: res.response});
          }
        )
      }
    );
  }

  getPP() {
    return this.getUser().then(res => {
      return Api.databases().getDocument('6388eaa9180af7d3aec7', '6388eab14a30e493837d', String(res.$id));
    });
  }

  getUser() {
    return Api.accounts().get();
  }
}
