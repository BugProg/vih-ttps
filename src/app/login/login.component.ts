import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {LoginService} from '../services/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  email: string = '';
  password: string = '';

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
  }

  onLogin() {
    if (this.email.length <= 0 && this.password.length <= 0) {
     return;
    }
    this.loginService.log(this.email, this.password).then(
      res => {
        console.log('Logged ', res);
        this.router.navigate(['/']);
      }, error => {
        console.log('error');
      }
    );
  }

}
