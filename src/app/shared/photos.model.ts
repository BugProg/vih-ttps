export class PhotosModel {
  public id: string;
  public imageURL: string;
  public comment: string;

  constructor(id: string, imageURL: string, comment: string) {
    this.id = id;
    this.imageURL = imageURL;
    this.comment = comment;
  }
}
