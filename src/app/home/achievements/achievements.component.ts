import {Component} from '@angular/core';
import SwiperCore, {Autoplay, EffectCoverflow, FreeMode, Pagination} from 'swiper';

SwiperCore.use([Autoplay, EffectCoverflow, FreeMode, Pagination]);

@Component({
  selector: 'app-achievements',
  templateUrl: './achievements.component.html',
  styleUrls: ['./achievements.component.css']
})
export class AchievementsComponent {

}
