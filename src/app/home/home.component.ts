import {Component, OnInit} from '@angular/core';

import {ContactDialogComponent} from '../contact/contact-dialog/contact-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {SeoService} from '../services/seo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public dialog: MatDialog, private seoService: SeoService) {
  }

  actionItem = [
    {
      title: 'HOME.actionItems.item1.title',
      subText: 'HOME.actionItems.item1.subText',
      icon: '../assets/icon/learn.svg',
      color: '#FDF0D6',
      alt: 'Logo renseignement'
    },
    {
      title: 'HOME.actionItems.item2.title',
      subText: 'HOME.actionItems.item2.subText',
      icon: '../assets/icon/protect.svg',
      color: '#DDF2FD',
      alt: 'Logo protection'
    },
    {
      title: 'HOME.actionItems.item3.title',
      subText: 'HOME.actionItems.item3.subText',
      icon: '../assets/icon/detect.svg',
      color: '#FDE7EF',
      alt: 'Logo dépistage'
    }];

  workProcess = [
    {
      title: 'HOME.workProcess.item1.title',
      text: 'HOME.workProcess.item1.text'
    },
    {
      title: 'HOME.workProcess.item2.title',
      text: 'HOME.workProcess.item2.text'
    },
    {
      title: 'HOME.workProcess.item3.title',
      text: 'HOME.workProcess.item3.text'
    },
    {
      title: 'HOME.workProcess.item4.title',
      text: 'HOME.workProcess.item4.text'
    },
    {
      title: 'HOME.workProcess.item5.title',
      text: 'HOME.workProcess.item5.text'
    }
  ]

  badges = [
    {
      name: 'ESIEA',
      imagePath: '../assets/images/badge/esiea.jpg'
    },
    {
      name: 'IUT',
      imagePath: '../assets/images/badge/iut.jpg'
    },
    {
      name: 'Logo Chauffage',
      imagePath: '../assets/images/badge/night.jpg'
    }
  ]

  ngOnInit(): void {
    this.seoService.generateTags({
      title: 'TTPS',
      description: 'ABC du Plombier est une entreprise Rezéenne spécialisée dans la plomberie et la pose de chaudière gaz. Nous rayonnons autour de l’agglomération nantaise. Appelez le 06 40 37 96 23 pour un devis gratuit !'
    })
  }

  onContact() {
    this.dialog.open(ContactDialogComponent);
  }

}
