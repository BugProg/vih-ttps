import {Component, ViewEncapsulation} from '@angular/core';
import SwiperCore, {Autoplay, Navigation} from 'swiper';

SwiperCore.use([Navigation, Autoplay]);

export interface Review {
  author_name: string;
  profile_photo_url: string;
  rating: number;
  text: string;
}

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ReviewsComponent {
}
