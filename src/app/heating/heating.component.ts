import {Component, OnInit} from '@angular/core';
import {SeoService} from '../services/seo.service';

@Component({
  selector: 'app-heating',
  templateUrl: './heating.component.html',
  styleUrls: ['./heating.component.css']
})
export class HeatingComponent implements OnInit {

  actionItem = [
    {
      title: 'HEATING.section1.item1.title',
      subText: 'HEATING.section1.item1.text',
      icon: '../assets/icon/boiler-repair.png',
    },
    {
      title: 'HEATING.section1.item2.title',
      subText: 'HEATING.section1.item2.text',
      icon: '../assets/icon/boiler-installation.png',
    },
    {
      title: 'HEATING.section1.item3.title',
      subText: 'HEATING.section1.item3.text',
      icon: '../assets/icon/boiler-settings.png',
    }];

  constructor(private seoService: SeoService) {
  }

  ngOnInit(): void {
    this.seoService.generateTags({
      title: 'Chauffage',
      description: 'ABC du Plombier fournit des services de haute qualité dans la région de Nantes. Nous offrons une garantie de satisfaction à 100 %. Appelez-nous ou demandez votre devis en ligne !'
    });
  }

}
