import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';

interface NavigationSections {
  sectionName: string;
  links: {
    text: string,
    hrefAttribute?: string,
    routerAttribute?: string
  }[]
}

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  sections: NavigationSections[] = [
    {
      sectionName: 'Contact',
      links: [
        {
          text: '190 boulevard Charonne 75020 PARIS',
          hrefAttribute: 'https://www.qwant.com/maps/place/addr:2.392437;48.857853:190@190_Boulevard_de_Charonne#map=16.50/48.8578530/2.3924370',
        },
        {
          text: '0 800 840 800',
          hrefAttribute: 'tel:+33800840800'
        },
        {
          text: 'https://www.sida-info-service.org',
          hrefAttribute: 'https://www.sida-info-service.org'
        }
      ]
    },

    {
      sectionName: 'Information',
      links: [
        {
          text: 'Dépôt Gitlab',
          hrefAttribute: 'https://gitlab.com/abc-du-plombier1'
        }
      ]
    },

    {
      sectionName: 'Navigation',
      links: [
        {
          text: 'Accueil',
          routerAttribute: '/'
        },
        {
          text: 'Quiz',
          routerAttribute: '/quiz'
        },
        {
          text: 'Se faire dépister',
          routerAttribute: '/se-depister'
        },
        {
          text: 'Qui sommes nous ?',
          routerAttribute: '/qui-sommes-nous'
        }
      ]
    }
  ]

  constructor(private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  onPresentQuotationDialog() {
    window.open("https://google.fr/maps/search/CeGIDD/", "_blank");
  }

}
